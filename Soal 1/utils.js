function getRotationBetweenTwoPosition(x1, y1, x2, y2) {
    return Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
}

function getRandomFloatBetween(min, max) {
    return min + Math.random() * (max - min);
}

// https://stackoverflow.com/questions/11409895/whats-the-most-elegant-way-to-cap-a-number-to-a-segment
function clamp(num, min, max) {
    return num <= min ? min : num >= max ? max : num;
}

function angleToRad(angle) {
    return angle * Math.PI/180;
}