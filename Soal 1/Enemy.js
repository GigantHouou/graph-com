"use strict";

class Enemy1 extends GameObj {
    constructor() {
        super();

        this.boundBoxSize = [40, 40];
        this.origin = [20, 20];
        this.objType = "enemy1";

        this.health = 1;
        this.speed = 0;

        this.drawList = [
            ["triangles", [255, 148, 148, 1], [
                0, 0,
                40, 0,
                40, 40,
                0, 0,
                0, 40,
                40, 40
            ]]
        ];
    }
}

class Enemy2 extends GameObj {
    constructor() {
        super();

        this.boundBoxSize = [50, 50];
        this.origin = [25, 25];
        this.objType = "enemy2";

        this.health = 2;
        this.speed = 0;

        this.drawList = [
            ["triangles", [218, 82, 82, 1], [
                0, 0,
                50, 0,
                50, 50,
                0, 0,
                0, 50,
                50, 50
            ]]
        ];
    }
}

class Enemy3 extends GameObj {
    constructor() {
        super();

        this.boundBoxSize = [60, 60];
        this.origin = [30, 30];
        this.objType = "enemy3";

        this.health = 3;
        this.speed = 0;

        this.drawList = [
            ["triangles", [149, 53, 53, 1], [
                0, 0,
                50, 0,
                50, 50,
                0, 0,
                0, 50,
                50, 50
            ]]
        ];
    }
}