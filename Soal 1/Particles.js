"use strict";

class PlayerBulletSplash extends GameObj{
    constructor() {
        super();

        this.boundBoxSize = [20, 20];
        this.origin = [10, 10];
        this.objType = "particles";

        this.drawList = [
            ["triangle_fan", [148, 191, 255, 1], [
                10, 10,
                10, 0,
                17, 3,
                20, 10,
                17, 17,
                10, 20,
                3, 17,
                0, 10,
                3, 3,
                10, 0
            ]]
        ];
    }
}

class Enemy1DeathSplash extends GameObj{
    constructor() {
        super();

        this.boundBoxSize = [20, 20];
        this.origin = [10, 10];
        this.objType = "particles";

        this.drawList = [
            ["triangles", [255, 148, 148, 1], [
                0, 0,
                20, 0,
                10, 20,
            ]]
        ];
    }
}

class Enemy2DeathSplash extends GameObj{
    constructor() {
        super();

        this.boundBoxSize = [25, 25];
        this.origin = [12.5, 12.5];
        this.objType = "particles";

        this.drawList = [
            ["triangles", [218, 82, 82, 1], [
                0, 0,
                25, 0,
                12.5, 25,
            ]]
        ];
    }
}

class Enemy3DeathSplash extends GameObj{
    constructor() {
        super();

        this.boundBoxSize = [30, 30];
        this.origin = [15, 15];
        this.objType = "particles";

        this.drawList = [
            ["triangles", [149, 53, 53, 1], [
                0, 0,
                30, 0,
                15, 30,
            ]]
        ];
    }
}