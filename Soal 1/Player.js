"use strict";

class PlayerTurret extends GameObj {
    constructor() {
        super();

        this.boundBoxSize = [16, 108];
        this.origin = [8, 56];
        this.objType = "player";

        this.drawList = [
            ["triangles", [242, 242, 242, 1], [
                0, 0,
                16, 0,
                16, 108,
                0, 0,
                0, 108,
                16, 108
            ]],
            ["triangles", [148, 191, 255, 1], [
                0, 0,
                16, 0,
                16, 56,
                0, 0,
                0, 56,
                16, 56
            ]]
        ];
    }
}

class PlayerTurretBody extends GameObj {
    constructor() {
        super();

        this.boundBoxSize = [86, 24];
        this.origin = [43, 24];
        this.objType = "player";

        this.drawList = [
            ["triangles", [148, 191, 255, 1], [
                20, 0,
                66, 0,
                86, 24,
                20, 0,
                0, 24,
                86, 24
            ]]
        ];
    }
}

class PlayerWall extends GameObj {
    constructor() {
        super();

        this.boundBoxSize = [1000, 20];
        this.origin = [500, 20];
        this.objType = "player";

        this.drawList = [
            ["triangles", [186, 214, 255, 1], [
                0, 0,
                1000, 0,
                1000, 20,
                0, 0,
                0, 20,
                1000, 20
            ]]
        ];
    }
}

class PlayerBullet extends GameObj {
    constructor() {
        super();

        this.boundBoxSize = [20, 20];
        this.origin = [10, 10];
        this.objType = "playerBullet";

        this.drawList = [
            ["triangle_fan", [148, 191, 255, 1], [
                10, 10,
                10, 0,
                17, 3,
                20, 10,
                17, 17,
                10, 20,
                3, 17,
                0, 10,
                3, 3,
                10, 0
            ]]
        ];
    }
}