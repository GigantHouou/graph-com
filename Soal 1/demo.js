"use strict";

var canvas;
var gl;
	
var colorUniformLocation;
var matrixLocation;
var matrix;
var count;

var translationMatrix;
var rotationMatrix;
var scaleMatrix;
var projectionMatrix;
var moveOriginMatrix;

var canvasWidth;
var canvasHeight;
var middleWidth;
var middleHeight;

var time;
var refreshRate = 1000/40;

var playerList = [];
var playerBulletList = [];
var enemyList = [];
var playerBulletSplashList = [];
var enemyDeathSplashList = [];

var scoreBar;
var healthBar;

// latest drawn object is rendered in front
var objList = [
    playerBulletList,
    enemyList,
    playerList,
    enemyDeathSplashList,
    playerBulletSplashList
];

var playerTurret;

var playerBulletSpeed = 12;
var playerFireRate = 160;
var nextPlayerFire;
var holdButtonFunction;

var enemySpawnRate = [200, 1000];
var nextEnemySpawn;

var turretRotateRight;

window.onload = function init()
{
    canvas = document.getElementById("play-canvas");
    gl = WebGLUtils.setupWebGL( canvas );
    if (!gl) { alert("WebGL isn't available"); }

    // Configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(242/255, 242/255, 242/255, 1.0);

    // Load shaders and initialize attribute buffers
    let program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    // Load the data into the GPU
    let objectBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, objectBuffer);
	
    // Associate out shader variables with our data buffer
    let vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

	colorUniformLocation = gl.getUniformLocation(program, "u_color");
    matrixLocation = gl.getUniformLocation(program, "u_matrix");
    
    canvasWidth = gl.canvas.width;
    canvasHeight = gl.canvas.height;
    middleWidth = Math.floor(gl.canvas.width/2);
    middleHeight = Math.floor(gl.canvas.height/2);

    time = 0;

    // Start game logic here

    // Player init
    playerTurret = new PlayerTurret();
    let playerTurretBody = new PlayerTurretBody();
    let playerWall = new PlayerWall();

    playerTurret.position = [middleWidth, canvasHeight - 20];
    playerTurretBody.position = [middleWidth, canvasHeight - 20];
    playerWall.position = [middleWidth, canvasHeight];

    playerList.push(playerTurret, playerTurretBody, playerWall);

    /*canvas.addEventListener("mousemove", function(e){
        playerTurret.rotation = clamp(getRotationBetweenTwoPosition(
            e.clientX - (window.screen.width/2 - middleWidth), e.clientY - 6, playerTurret.position[0], playerTurret.position[1]
        ) - 90, -87, 87);
        if (playerTurret.rotation == -87 && e.clientX - (window.screen.width/2 - middleWidth) > playerTurret.position[0]) playerTurret.rotation = 87;
    });

    let holdButtonFunction = setInterval(function(){}, 1000);
    clearInterval(holdButtonFunction);
    canvas.addEventListener("mousedown", function(e){
        spawnPlayerBullet();
        holdButtonFunction = setInterval(function() { spawnPlayerBullet() }, playerFireRate);
    });

    canvas.addEventListener("mouseup", function(e){
        clearInterval(holdButtonFunction);
    });*/
    setInterval(function() { spawnPlayerBullet() }, playerFireRate);

    let displays = document.querySelectorAll("p");
    scoreBar = displays[0];
    healthBar = displays[1];

    nextEnemySpawn = 0;
    nextPlayerFire = 0;

    turretRotateRight = true;

    render();
}
//score and health points
var score = 0;
var health = 100;

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    // Spawn enemy and when health < 0, enemy won't respawn 
    if (health > 0 && time >= nextEnemySpawn){
        spawnEnemy();
        nextEnemySpawn = time + getRandomFloatBetween(enemySpawnRate[0], enemySpawnRate[1]);
    }

    // Enemy
    enemyList.forEach(obj => {
        obj.position[1] += obj.speed;
        playerList.forEach(playerObj => {
            if (checkOverlap(obj, playerObj)) {
                for (let i = 0; i < 10; i++) {
                    if (obj.objType == "enemy1")  addEnemyDeathSplash(obj.position[0], obj.position[1], "enemy1");
                    else if (obj.objType == "enemy2") addEnemyDeathSplash(obj.position[0], obj.position[1], "enemy2");
                    else if (obj.objType == "enemy3") addEnemyDeathSplash(obj.position[0], obj.position[1], "enemy3");
                }
                enemyList.splice(enemyList.indexOf(obj), 1);
                /*if (obj.objType == "enemy1") health -= 1;
                else if (obj.objType == "enemy2") health -= 3;
                else if (obj.objType == "enemy3") health -= 5;
                healthBar.innerHTML = health;*/
            }
        });
    });

    // Player bullet
    playerBulletList.forEach(obj => {
        obj.position = [obj.position[0] + Math.sin(angleToRad(obj.rotation + 5)) * playerBulletSpeed,
                        obj.position[1] - Math.cos(angleToRad(obj.rotation + 5)) * playerBulletSpeed];
        if (obj.position[0] < -50 || obj.position[0] > canvasWidth + 50 || obj.position[1] < -50 || obj.position[1] > canvasHeight + 50) {
            playerBulletList.splice(playerBulletList.indexOf(obj), 1);
        }
        enemyList.forEach(enemy => {
            if (checkOverlap(obj, enemy)) {
                // Player bullet particles init
                for (let i = 0; i < 15; i++) {
                    addPlayerBulletSplash(obj.position[0], obj.position[1]);
                }

                enemy.health -= 1;
                if (enemy.health <= 0) {
                    // Enemy death particles init
                    for (let i = 0; i < 10; i++) {
                        if (enemy.objType == "enemy1")  addEnemyDeathSplash(enemy.position[0], enemy.position[1], "enemy1");
                        else if (enemy.objType == "enemy2") addEnemyDeathSplash(enemy.position[0], enemy.position[1], "enemy2");
                        else if (enemy.objType == "enemy3") addEnemyDeathSplash(enemy.position[0], enemy.position[1], "enemy3");
                    }
                    enemyList.splice(enemyList.indexOf(enemy), 1);
                }

                playerBulletList.splice(playerBulletList.indexOf(obj), 1);
                if (enemy.objType == "enemy1") score += 1;
                else if (enemy.objType == "enemy2") score += 3;
                else if (enemy.objType == "enemy3") score += 5;
                /// scoreBar.innerHTML = score;
            }
        });
    });

    // Player bullet particles anim
    playerBulletSplashList.forEach(obj => {
        obj.position = [obj.position[0] + Math.sin(angleToRad(obj.rotation)) * 3,
                        obj.position[1] - Math.cos(angleToRad(obj.rotation)) * 3];
        obj.scale = [obj.scale[0] - 0.05, obj.scale[1] - 0.05];
        if (obj.scale[0] < 0) obj.scale[0] = 0;
        if (obj.scale[1] < 0) obj.scale[1] = 0;
    });

    // Enemy death particles anim
    enemyDeathSplashList.forEach(obj => {
        obj.position = [obj.position[0] + Math.sin(angleToRad(obj.rotation)) * 4,
                        obj.position[1] - Math.cos(angleToRad(obj.rotation)) * 4];
        obj.scale = [obj.scale[0] - 0.02, obj.scale[1] - 0.02];
        if (obj.scale[0] < 0) obj.scale[0] = 0;
        if (obj.scale[1] < 0) obj.scale[1] = 0;
    });

    if (turretRotateRight) playerTurret.rotation += 5;
    else playerTurret.rotation -= 5;

    if (playerTurret.rotation > 80) turretRotateRight = false;
    else if (playerTurret.rotation < -80) turretRotateRight = true;

    drawAll();
    setTimeout(function() { requestAnimFrame(render); }, refreshRate);
    time += refreshRate;
    if(health <= 0){
        restartGame();
    }
}

//restart option when health = 0
function restartGame() {
    let r = confirm("Game over! Do you want to try again?");
    //end game state
    health = -1;
    //reload page 
    if (r == true) {
        location.reload();
    }
    else {
        return;
    }
    document.getElementById("demo").innerHTML = txt;
}

function spawnEnemy() {
    let rand = Math.random();
    let newEnemy;
    if (rand > 0.4) newEnemy = new Enemy1();
    else if (rand < 0.2) newEnemy = new Enemy2();
    else newEnemy = new Enemy3();
    newEnemy.speed = getRandomFloatBetween(3, 7);
    
    newEnemy.position = [getRandomFloatBetween(80, canvasWidth - 80), -80];
    enemyList.push(newEnemy);
}

function spawnPlayerBullet() {
    if (time >= nextPlayerFire){
        let newPlayerBullet = new PlayerBullet();
        newPlayerBullet.position = [playerTurret.position[0] + Math.sin(angleToRad(playerTurret.rotation + 2)) * playerTurret.boundBoxSize[1]/2,
                                    playerTurret.position[1] - Math.cos(angleToRad(playerTurret.rotation + 2)) * playerTurret.boundBoxSize[1]/2];
        newPlayerBullet.rotation = playerTurret.rotation - 5;
        playerBulletList.push(newPlayerBullet);
        nextPlayerFire = time + playerFireRate;

        for (let i = 0; i < 5; i++) {
            addPlayerBulletSplash(newPlayerBullet.position[0], newPlayerBullet.position[1]);
        }
    }
}

function drawAll() {
    for (let i = 0; i < objList.length; i++) {
        for (let j = 0; j < objList[i].length; j++) {
            for (let k = 0; k < objList[i][j].drawList.length; k++) {
                draw(objList[i][j], objList[i][j].drawList[k]);
            }
        }
    }
}

function draw(obj, drawObj) {
    let drawType = drawObj[0];
    let drawColor = drawObj[1];
    let drawVertices = drawObj[2];

    count = drawVertices.length/2;
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(drawVertices), gl.STATIC_DRAW);
    
    matrix = m3.identity();
    projectionMatrix = m3.projection(canvasWidth, canvasHeight);
    translationMatrix = m3.translation(obj.position[0], obj.position[1]);
    scaleMatrix = m3.scaling(obj.scale[0], obj.scale[1]);
    rotationMatrix = m3.rotation(angleToRad(obj.rotation));
    moveOriginMatrix = m3.translation(-obj.origin[0], -obj.origin[1]);
    
    matrix = m3.multiply(projectionMatrix, translationMatrix);
    matrix = m3.multiply(matrix, scaleMatrix);
    matrix = m3.multiply(matrix, rotationMatrix);
    matrix = m3.multiply(matrix, moveOriginMatrix);

    gl.uniform4f(colorUniformLocation, drawColor[0]/255, drawColor[1]/255, drawColor[2]/255, drawColor[3]);
    gl.uniformMatrix3fv(matrixLocation, false, matrix);

    switch(drawType) {
        case "triangles":
            gl.drawArrays(gl.TRIANGLES, 0, count);
            break;
        case "triangle_fan":
            gl.drawArrays(gl.TRIANGLE_FAN, 0, count);
            break;
    }
}

// work if the origin of both object is in the middle point of its bound box
function checkOverlap(obj1, obj2) {
    if (Math.abs(obj1.position[0] - obj2.position[0]) < obj1.boundBoxSize[0]/2 + obj2.boundBoxSize[0]/2 &&
    Math.abs(obj1.position[1] - obj2.position[1]) < obj1.boundBoxSize[1]/2 + obj2.boundBoxSize[1]/2) {
        return true;
    }
    else {
        return false;
    }
}

function addPlayerBulletSplash(x, y) {
    let newParticle = new PlayerBulletSplash();
    newParticle.position = [getRandomFloatBetween(x - 15, x + 15),
                            getRandomFloatBetween(y - 15, y + 15)];
    let randomScale = getRandomFloatBetween(0.4, 0.8);
    newParticle.scale = [randomScale, randomScale];
    newParticle.rotation = getRotationBetweenTwoPosition(x, y, newParticle.position[0], newParticle.position[1]);
    playerBulletSplashList.push(newParticle);
    setTimeout(function() { playerBulletSplashList.splice(playerBulletSplashList.indexOf(newParticle), 1); }, 300);
}

function addEnemyDeathSplash(x, y, enemyType) {
    let newParticle;
    if (enemyType == "enemy1") newParticle = new Enemy1DeathSplash();
    else if (enemyType == "enemy2") newParticle = new Enemy2DeathSplash();
    else if (enemyType == "enemy3") newParticle = new Enemy3DeathSplash();
    newParticle.position = [getRandomFloatBetween(x - 25, x + 25),
                            getRandomFloatBetween(y - 25, y + 25)];
    let randomScale = getRandomFloatBetween(0.5, 1.2);
    newParticle.scale = [randomScale, randomScale];
    newParticle.rotation = getRotationBetweenTwoPosition(x, y, newParticle.position[0], newParticle.position[1]);
    enemyDeathSplashList.push(newParticle);
    setTimeout(function() { enemyDeathSplashList.splice(enemyDeathSplashList.indexOf(newParticle), 1); }, 1000);
}