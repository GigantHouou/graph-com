"use strict";

class GameObj {
    constructor(){
        // [x, y]
        this.position = [0, 0];
        this.scale = [1, 1];
        this.rotation = 0;

        this.boundBoxSize = [0, 0];
        this.origin = [0, 0];
        this.objType = "object";

        // [[draw type, color, vertices], ...]
        this.drawList = [];
    }
}