# UTS Grafika Komputer

Kelompok: Graph-Com

Anggota:
- Andika Hanavian Atmam (1606918585)
- Jovanta Anugerah Pelawi (1606875863)
- Muzakki Hassan Azmi (1606917632)

Konstribusi/pembagian kerja untuk kelompok ini:
- Soal 1 (Muzakki Hassan Azmi dan Jovanta Anugerah Pelawi)
- Soal 2 (Andika Hanavian Atmam)

Meskipun utamanya seperti itu, semua anggota kelompok tidak hanya 
fokus kepada satu soal saja, tetapi juga fokus ke soal lain sebagai
ajang untuk saling membantu jika ada yang kesusahan.

------------------------------------------------------

## Soal 1 (Animasi/game sederhana)

Game: Pertahanan Melawan Kotak Jahat

Deskripsi: Sebuah permainan shooter sederhana dimana pemain mengendalikan turret yang diam di satu tempat.

Objek:
- Turret dan wall: wall (2 segitiga), badan turret (2 segitiga), turret (2 segitiga)
- Musuh (2 segitiga)
- Peluru turret (8 segitiga)
- Efek partikel: splash effect dari peluru (8 segitiga), efek musuh hancur (1 segitiga)

Transformasi:
- Setiap kumpulan objek tertentu disimpan dalam bentuk class yang memiliki nilai-nilai transformasi seperti posisi, rotasi, dan scale
- Turret: rotasi (menggunakan fungsi yang mengambil posisi 2 vektor, antara mouse dan turret, untuk dicari nilai rotasinya)
- Musuh: translasi (atas ke bawah)
- Peluru turret: translasi (tergantung arah mouse, dihitung dengan cara mengambil nilai rotasi turret, lalu nilai rotasi tersebut digunakan untuk menentukan arah dan pergerakannya)
- Efek partikel: translasi (bergerak menjauh dari titik tengah particle group, dihitung dengan cara mencari nilai rotasi antara titik tengah particle group dan posisi awal partikel, lalu nilai rotasi tersebut digunakan untuk menentukan arah dan pergerakannya)
- Mendeteksi overlap dengan membandingkan x dan y jarak antar 2 objek dan jumlah dari setengah dari kedua bounding box objek tersebut (asumsi kedua objek tersebut memiliki titik origin tepat di tengah)

Cara bermain:
- Jalankan game dengan membuka game.html, untuk demo buka demo.html
- Pemain dapat mengarahkan arah tembak turret menggunakan mouse, lalu klik/hold mouse kiri untuk menembakkan peluru
- Turret tidak bisa berpindah tempat
- Musuh akan mengurangi health pemain jika menyentuh wall di bawah
- Pemain mendapatkan skor setelah menghancurkan musuh dengan peluru
- Health awal pemain 100 dan akan game over jika health tersebut habis
- Tekan tombol restart untuk mengulang permainan
- Tujuan dari permainan ini adalah mendapatkan skor setinggi mungkin

Tipe musuh:
- Tipe 1, butuh ditembak sekali agar hancur, jika menyentuh tanah akan -1 health pemain, jika dihancurkan akan +1 skor
- Tipe 2, butuh ditembak 2 kali agar hancur, jika menyentuh tanah akan -3 health pemain, jika dihancurkan akan +3 skor
- Tipe 3, butuh ditembak 3 kali agar hancur, jika menyentuh tanah akan -5 health pemain, jika dihancurkan akan +5 skor

------------------------------------------------------

## Soal 2 (26 Kamera dan animasi teapot)

Untuk Posisi Kamera, dimanfaatkanlah dan juga sumbu x dan y serta theta yang disediakan langsung di dalam teapot4.js.
Sebelum membuat penukaran posisi kamera, ada tombol bersama fungsi yang mengubah arah rotasi (x, y, dan, z) yang ada di dalamnya.

Selanjutnya, untuk melakukan pemindahan kamera, digunakanlah arah panah di mana:
- panah kiri: nilai sumbu y akan bertambah
- panah atas: nilai sumbu x akan bertambah
- panah kanan: nilai sumbu y akan berkurang
- panah bawah: nilai sumbu x akan berkurang

(Catatan: masing-masing akan ditambah atau dikurangi dengan theta sebesar 1.5)

Selain default case yang dijelaskan di atas, ada ketentuan khusus jika menekan tombol dan 
akhirnya sampai di titik atas atau bawah, tombol tersebut (atas/bawah) tidak berfungsi 
kepada sudut kiri/kanan atas dan sebagainya.

